from io import BytesIO

from rainbow_phantom.pil_extension import ExtendedImage, Pixel
from rainbow_phantom.settings import EXAMPLE_IMAGE


FOLDER = './output_images'
WHITE_RGB = (255, 255, 255)


image = ExtendedImage.open(EXAMPLE_IMAGE)
for pixel in image.iter_pixels():
    _r, _g, _b, alpha = pixel.value
    if alpha != 0:
        image.putpixel(Pixel(pixel.x, pixel.y, WHITE_RGB + (alpha, )))
buffer = BytesIO()
image.save(buffer, 'PNG')
buffer.seek(0)
with open(f'{FOLDER}/white_example_output.png', 'wb') as out_file:
    out_file.write(buffer.read())
