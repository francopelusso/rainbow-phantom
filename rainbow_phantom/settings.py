import os

from environs import Env


ENV_FOLDER = '../' if not os.path.exists('.env') else ''
ENV_FILENAME = '.env'
env = Env()
env.read_env(f'{ENV_FOLDER}{ENV_FILENAME}')


ENVIRONMENT = env('ENVIRONMENT', 'production')
ROOT_PATH = '' if ENVIRONMENT == 'local' else '/rp'
BASE_FOLDER = '' if os.getcwd() == 'rainbow_phantom' else 'rainbow_phantom'
IMAGES_FOLDER = os.path.join(BASE_FOLDER, env('IMAGES_FOLDER', 'images'))
GHOST_IMAGE = env('GHOST_IMAGE', 'pacman_ghost.png')
EXAMPLE_IMAGE = os.path.join(IMAGES_FOLDER, GHOST_IMAGE)
