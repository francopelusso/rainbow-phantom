from __future__ import annotations
from dataclasses import dataclass
from typing import Generator, Tuple

from PIL import Image

from rainbow_phantom.settings import EXAMPLE_IMAGE


PNG_IMAGE = Image.open(EXAMPLE_IMAGE)

IMAGE_ATTRIBUTES = [att for att in dir(PNG_IMAGE) if not callable(getattr(PNG_IMAGE, att))]


@dataclass
class Pixel:
    # pylint: disable=invalid-name
    x: int
    y: int
    value: tuple


class ExtendedImage(Image.Image):
    # pylint: disable=abstract-method

    @classmethod
    def open(cls, fp, mode='r'):  # pylint: disable=invalid-name
        return cls.from_pil_image(Image.open(fp, mode))

    @classmethod
    def from_pil_image(cls, pil_image: Image.Image) -> ExtendedImage:
        image = cls()
        for att in IMAGE_ATTRIBUTES:
            try:
                setattr(image, att, getattr(pil_image, att, None))
            except AttributeError as e:
                if str(e) == "can't set attribute":
                    IMAGE_ATTRIBUTES.remove(att)
        image.im = pil_image.im
        return image

    def getpixel(self, xy: Tuple[int, int]) -> Pixel:
        return Pixel(xy[0], xy[1], super().getpixel(xy))

    def iter_pixels(self) -> Generator[Pixel, None, None]:
        width, height = self.size
        for x_cord in range(width):
            for y_cord in range(height):
                yield self.getpixel((x_cord, y_cord))

    def putpixel(self, pixel: Pixel):  # pylint: disable=arguments-differ
        super().putpixel((pixel.x, pixel.y), pixel.value)

    def resize(self, size, resample=Image.BICUBIC, box=None, reducing_gap=None):
        keep_ratio = None in size
        width, height = size
        ratio = self.size[0] / self.size[1]
        if keep_ratio and size[0] is None:
            width = round(height * ratio)
        elif keep_ratio and size[1] is None:
            height = round(width / ratio)
        pil_image = super().resize((width, height), resample, box, reducing_gap)
        return self.from_pil_image(pil_image)
