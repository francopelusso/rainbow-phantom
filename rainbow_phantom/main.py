import os
from io import BytesIO

from fastapi import FastAPI
from fastapi.responses import Response

from rainbow_phantom.pil_extension import ExtendedImage, Pixel
from rainbow_phantom.settings import ROOT_PATH, GHOST_IMAGE, IMAGES_FOLDER


app = FastAPI(
    title='Rainbow Phantom',
    description='Paint your ghost any colour you want!',
    version='0.1',
    docs_url='/',
    root_path=ROOT_PATH,
    root_path_in_servers=False,
    openapi_tags=[{
        'name': 'rainbow',
        'description': 'Come in colours everywhere, coming colours on the air!',
        'externalDocs': {
            'description': 'They shoot colors all around like a sunset going down.',
            'url': 'https://www.youtube.com/watch?v=R2U8htL3ZtY'
        }
    }]
)


@app.get('/ghosties/', tags=['rainbow'], response_class=Response(media_type='image/png'))
def get_coloured_ghost(red: int = 0, green: int = 0, blue: int = 0, width: int = None,
                       height: int = None):
    content = BytesIO()
    image = ExtendedImage.open(os.path.join(IMAGES_FOLDER, GHOST_IMAGE))
    if width is not None or height is not None:
        image = image.resize((width, height))
    rgb = (red, green, blue)
    for pixel in image.iter_pixels():
        _r, _g, _b, alpha = pixel.value
        if alpha != 0:
            image.putpixel(Pixel(pixel.x, pixel.y, rgb + (alpha, )))
    image.save(content, 'PNG')
    content.seek(0)
    return Response(content.read(), media_type='image/png')
